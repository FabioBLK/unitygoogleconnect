﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;
using Organizer;

public class FileOrganizer : EditorWindow
{
    private string JSONFilesPath = "Assets/Resources/JSON";
    private string JSONFileExtension = "json";
    private string credentialsFilePath = "Assets/Resources";
    private string credentialsFileName = "credentials.json";
    private string googleSheetFileId = string.Empty; // Fabio's Sample = "1P59AgZsLToznYY02Ticmmvw88zGk3jqg_bg-U9WWNxU";
    private string googleSheetRange = string.Empty;  // Fabio's Sample = "TestTab!A2:B";
    private string fileNameNewPefix = "File";
    private string sheetData = string.Empty;
    private string alert = string.Empty;

    private int uiemptySpace = 15;

    private List<string> ids = new List<string>();
    private Dictionary<string, string> toRenameDict = new Dictionary<string, string>();
    private Dictionary<string, string> fileIdsDict = new Dictionary<string, string>();

    [MenuItem("Tools/File Organizer")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(FileOrganizer));
    }

    private void OnGUI()
    {
        GUILayout.Label("File Organizer", EditorStyles.boldLabel);

        GUILayout.Space(uiemptySpace);
        JSONFilesPath = EditorGUILayout.TextField("JSON Files Path", JSONFilesPath);
        GUILayout.Label("Ex.: Assets/Resources/JSON", EditorStyles.helpBox);

        GUILayout.Space(uiemptySpace);
        credentialsFilePath = EditorGUILayout.TextField("Credentials Files Path", credentialsFilePath);
        GUILayout.Label("Ex.: Assets/Resources", EditorStyles.helpBox);

        GUILayout.Space(uiemptySpace);
        credentialsFileName = EditorGUILayout.TextField("Credentials File Name", credentialsFileName);
        GUILayout.Label("Ex.: credentials.json", EditorStyles.helpBox);

        GUILayout.Space(uiemptySpace);
        googleSheetFileId = EditorGUILayout.TextField("Google Sheet File ID", googleSheetFileId);
        GUILayout.Label("Ex.: 1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms", EditorStyles.helpBox);

        GUILayout.Space(uiemptySpace);
        googleSheetRange = EditorGUILayout.TextField("Sheet Range", googleSheetRange);
        GUILayout.Label("Ex.: Class Data!A2:E", EditorStyles.helpBox);

        GUILayout.Space(uiemptySpace);
        fileNameNewPefix = EditorGUILayout.TextField("File Name New Prefix", fileNameNewPefix);
        GUILayout.Label("Ex.: File", EditorStyles.helpBox);

        GUILayout.Space(uiemptySpace);
        if (GUILayout.Button("Run"))
        {
            fileIdsDict = new Dictionary<string, string>();
            toRenameDict = new Dictionary<string, string>();
            PopulateJsonFile();
            ids = new List<string>();
            alert = "Running";
            sheetData = string.Empty;
            LoadSheetData();
        }

        GUILayout.Space(uiemptySpace);
        GUILayout.Label("Output Info", EditorStyles.boldLabel);
        GUILayout.TextArea(alert);

        GUILayout.Space(uiemptySpace);
        GUILayout.Label("Google Sheet Preview Header", EditorStyles.label);
        GUILayout.TextArea(sheetData);
    }

    private void PopulateJsonFile()
    {
        string[] files = Directory.GetFiles(string.Format("{0}", JSONFilesPath), string.Format("*.{0}", JSONFileExtension));
        for (int i = 0; i < files.Length; i++)
        {
            StreamReader reader = new StreamReader(files[i]);
            LayoutJSON jsonFile = JsonUtility.FromJson<LayoutJSON>(reader.ReadToEnd());
            if (!fileIdsDict.ContainsKey(jsonFile.uid))
            {
                fileIdsDict.Add(jsonFile.uid, files[i]);
            }
            reader.Close();
        }
    }

    private void LoadSheetData()
    {
        if (File.Exists(string.Format("{0}/{1}", credentialsFilePath, credentialsFileName)))
        {
            // Gets data from remote spreadsheet
            SheetConnector sheetConnector = new SheetConnector(string.Format("{0}/{1}", credentialsFilePath, credentialsFileName));
            IList<IList<object>> data = sheetConnector.RequestData(googleSheetFileId, googleSheetRange);

            string output = string.Empty;
            if (data != null && data.Count > 0)
            {
                int header = 5;
                foreach (var value in data)
                {
                    if (header > 0)
                    {
                        sheetData += string.Format("uid : {0}\n", value[0]);
                        header--;
                    }

                    ids.Add(value[0].ToString());
                }

                // Check for duplicates on spreadsheet
                string duplicateId = DuplicatedId(ids);
                if (!string.IsNullOrEmpty(duplicateId))
                {
                    ShowAlert(string.Format("The Spreadsheet has a duplicated Key - {0}", duplicateId));
                    return;
                }

                for (int i = 0; i < ids.Count; i++)
                {
                    if (fileIdsDict.ContainsKey(ids[i].ToString()))
                    {
                        int fileIndex = i + 1;
                        PrepareToRename(fileIdsDict[ids[i].ToString()], string.Format("{0}{1}", fileNameNewPefix, fileIndex.ToString("D4")));
                    }
                    else
                    {
                        ShowAlert(string.Format("Invalid Key - {0}.\nThis key does not exists in any file", ids[i].ToString()));
                        return;
                    }
                }

                // File rename takes place here
                RunRenameBulk();

                // At this point, the app will find files that has ids 
                // not present on the spreadsheet to rename as a _noid file
                for (int i = 0; i < ids.Count; i++)
                {
                    if (fileIdsDict.ContainsKey(ids[i]))
                    {
                        fileIdsDict.Remove(ids[i]);
                    }
                }

                int index = 1;
                foreach (var file in fileIdsDict)
                {
                    RenameFile(file.Value, string.Format("_noid{0}", index.ToString("D4")));
                    index++;
                }

                // Renaming complete
                ShowAlert("Complete");
            }
            else
            {
                ShowAlert("File Empty or data invalid. Check your spreadsheet and range");
            }
        }
        else
        {
            ShowAlert("Credentials File not found. Make sure the file name is correct and inside the Resources Folder");
        }
    }

    private void RenameFile(string p_file, string p_newName)
    {
        string rename = string.Format("{0}/{1}.json", JSONFilesPath, p_newName);
        if (File.Exists(rename))
        {
            ShowAlert(string.Format("Error, you are trying to rename {0} into {1}. But the new file already exists.", p_file, rename));
            return;
        }

        if (File.Exists(p_file))
        {
            File.Move(p_file, rename);
            Debug.Log(string.Format("Success renaming {0} to {1}.", p_file, rename));
        }
        else
        {
            ShowAlert(string.Format("Error, you are trying to rename {0}. But this file does not exists.", p_file));
        }
    }

    private void PrepareToRename(string p_file, string p_newName)
    {
        if (!toRenameDict.ContainsKey(p_file))
        {
            toRenameDict.Add(p_file, p_newName);
        }
        else
        {
            ShowAlert(string.Format("Error, Invalid file name {0}. Check if you have duplicates on filesystem", p_file));
        }
    }

    private void RunRenameBulk()
    {
        foreach(var rename in toRenameDict)
        {
            RenameFile(rename.Key, string.Format("__{0}", rename.Value));
        }

        foreach(var rename in toRenameDict)
        {
            string fileRename = string.Format("{0}/__{1}.json", JSONFilesPath, rename.Value);
            RenameFile(fileRename, rename.Value);
        }
    }

    private string DuplicatedId(List<string> p_ids)
    {
        HashSet<string> idSet = new HashSet<string>();

        for (int i = 0; i < p_ids.Count; i++)
        {
            if (idSet.Contains(p_ids[i]))
            {
                return p_ids[i];
            }
        }

        return string.Empty;
    }

    private void ShowAlert(string p_infoString)
    {
        alert = p_infoString;
        Debug.LogWarning(alert);
    }

    private void LoadCredentialsFile()
    {
        using (var stream = new FileStream(credentialsFilePath + credentialsFileName, FileMode.Open, FileAccess.Read))
        {
            byte[] buffer = new byte[1024];
            int c;
            string output = string.Empty;

            while ((c = stream.Read(buffer, 0, buffer.Length)) > 0)
            {
                output += Encoding.UTF8.GetString(buffer, 0, c);
            }

            //Debug.LogWarning(output);
        }
    }
}

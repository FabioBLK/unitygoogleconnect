﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;

namespace Organizer
{
    public class SheetConnector
    {
        private string[] Scopes = { SheetsService.Scope.SpreadsheetsReadonly };
        private string ApplicationName = "Google Sheets API";
        private string TokenFilePath = "token.json";

        private SheetsService Service;

        public SheetConnector(string p_credentialsFilePath)
        {
            UserCredential credential;
            using (var stream = new FileStream(p_credentialsFilePath, FileMode.Open, FileAccess.Read))
            {
                string credPath = TokenFilePath;

                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
            }

            Service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });
        }

        public IList<IList<Object>> RequestData(string p_spreadsheetId, string p_sheetRange)
        {
            string spreadsheetId = p_spreadsheetId;
            string range = p_sheetRange;
            SpreadsheetsResource.ValuesResource.GetRequest request = Service.Spreadsheets.Values.Get(spreadsheetId, range);

            ValueRange response = request.Execute();
            IList<IList<Object>> values = response.Values;

            return values;
        }
    }
}


# Google Connector
## Download Links:

## To download the Full Document, follow this link: 
* https://1drv.ms/b/s!AsHRZHzUssaJiOxbHTC31DHdpgmmQg?e=5xcuRI

## To download the plugin, as an UnityPackage that can be imported into your current Unity Project:
* https://1drv.ms/u/s!AsHRZHzUssaJiOxWtP7HuDZrkgKxoA?e=G1tQbg

## To download a full Unity project that has the plugin and 5000 json files for test:
* https://1drv.ms/u/s!AsHRZHzUssaJiOxX7kUFtQnZMQxBtA?e=zgqP0F

## The google sheet file I used in this sample is here:
* https://docs.google.com/spreadsheets/d/1P59AgZsLToznYY02Ticmmvw88zGk3jqg_bg-U9WWNxU/edit#gid=0

Notice the id of this file is in the url. ID = 1P59AgZsLToznYY02Ticmmvw88zGk3jqg_bg-U9WWNxU

## You can also download the same Unity Sample Project from its Repository (You may need to share your Bitbucket account so I can allow your access)
* https://bitbucket.org/FabioBLK/unitygoogleconnect/src/master/
